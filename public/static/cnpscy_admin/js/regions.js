function delOperation(region_id, api_delete_url) {
    region_id = (region_id == '' || region_id == undefined || region_id == 'undefined' || isEmpty(region_id)) ? 0 : region_id;
    if (!isNumber(region_id) && region_id <= 0) {
        layerMsg('省市区Id必须为数字类型！', 5, layerJumpTime);
        return false;
    }
    layer.confirm('确定要将“' + $('tr#id_' + region_id).attr('name') + '”的记录进行“删除”操作吗？\b\b 删除之后将无法恢复！', {title: webTitle}, function () {
        axios.post( api_delete_url, {
            'region_id': region_id
        }).then(function(data){
            if (data.status == 1){
                layerMsg(data.msg, 1, layerJumpTime, 2);
            } else layerMsg(data.msg, 5, layerJumpTime);
        }).catch(function(res){
        });
    }, function () {
    });
}