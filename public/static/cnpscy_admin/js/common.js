/**
 * 新增与删除
 *
 * @param _this
 */
function saveOperation(_this) {
    var form_data = $(_this).serialize(),
        url = $(_this).attr('action');
    layerLoading();
    axios.post( url, form_data).then(function(data){
        if (data.status == 1) layerMsg(data.msg, 1, layerJumpTime, 3, $(_this).attr('return-url'));
        else layerMsg(data.msg, 5, layerJumpTime);
    }).catch(function(res){
    });
}

/**
 * 批量删除
 *
 * @param ajax_url
 * @returns {boolean}
 */
function batchDelOperation(api_url) {
    ids = [];
    for(i = 0; i < $('table input[type=checkbox]').length; i ++){
        if ($('table input[type=checkbox]').eq(i).prop('checked')) ids.push($('table input[type=checkbox]').eq(i).val());
    }
    if (ids.length <= 0) {
        layerMsg('请选择要进行批量删除的选项！', 5, 1000);
        return false;
    }
    layerLoading();
    layer.confirm('确定要进行“批量-软删除”操作吗？\b\b 删除之后将无法恢复！', {title: webTitle}, function () {
        axios.post( api_url, {'ids' : ids, 'is_batch' : 1}).then(function(data){
            if (data.status == 1){
                layerMsg(data.msg, 1, layerJumpTime, 2);
            } else layerMsg(data.msg, 5, layerJumpTime);
        }).catch(function(res){
        });
    }, function () {
    });
}

/**
 * 状态变更
 *
 * @param _this
 * @param url
 * @param _data
 */
function changeFiledStatus(_this, url, _data) {
    layerLoading();
    axios.post(url, _data).then(function (data) {
        switch (parseInt(data.status)) {
            case -1:
                layerMsg(data.msg, 5, layerJumpTime, 3, login_url);
            case 0:
                layerMsg(data.msg, 5, layerJumpTime);
                break;
            case 1:
                layerMsg(data.msg, 1, layerJumpTime, 2);;
                break;
            default:
                layerMsg(data.msg, 5);
                break;
        }
    }).catch(function (res) {
    });
}

function delOperation(id, api_delete_url) {
    id = (id == '' || id == undefined || id == 'undefined' || isEmpty(id)) ? 0 : id;
    if (!isNumber(id) && id <= 0) {
        layerMsg('Id必须为数字类型！', 5, layerJumpTime);
        return false;
    }
    layerLoading();
    layer.confirm('确定要将进行“删除”操作吗？\b\b 删除之后将无法恢复！', {title: webTitle}, function () {
        axios.post( api_delete_url, {
            'id': id
        }).then(function(data){
            if (data.status == 1){
                layerMsg(data.msg, 1, layerJumpTime, 2);
            } else layerMsg(data.msg, 5, layerJumpTime);
        }).catch(function(res){
        });
    }, function () {
    });
}

function batchExport(url) {
    hrefUrl(url);
}


function logout(url)
{
    layerLoading();

    localStorage.setItem("login_before_url", thisUrl());

    layer.confirm('确定要进行“退出登录”操作吗？', {title: webTitle}, function () {
        hrefUrl(url);
    }, function () {
    });
}