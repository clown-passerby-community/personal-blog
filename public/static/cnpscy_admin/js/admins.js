/**
 * [delOperation]
 * @author:cnpscy <[2278757482@qq.com]>
 * @chineseAnnotation:删除操作
 * @englishAnnotation:
 * @version:1.0
 * @param              {[type]} admin_id [description]
 * @return             {[type]}          [description]
 */
function delOperation(admin_id, api_delete_url) {
    admin_id = (admin_id == '' || admin_id == undefined || admin_id == 'undefined' || isEmpty(admin_id)) ? 0 : admin_id;
    if (!isNumber(admin_id) && admin_id <= 0) {
        layerMsg('管理员Id必须为数字类型！', 5, layerJumpTime);
        return false;
    }
    layer.confirm('确定要将管理员“' + $('tr#id_' + admin_id).attr('name') + '”进行“删除”操作吗？\b\b 删除之后将无法恢复！', {title: webTitle}, function () {
        axios.post( api_delete_url, {
            'admin_id': admin_id
        }).then(function(data){
            if (data.status == 1){
                layerMsg(data.msg, 1, layerJumpTime, 2);
            } else layerMsg(data.msg, 5, layerJumpTime);
        }).catch(function(res){
        });
    }, function () {
    });
}