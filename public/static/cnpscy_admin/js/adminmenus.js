/**
 * [delOperation]
 * @author:cnpscy <[2278757482@qq.com]>
 * @chineseAnnotation:删除操作
 * @englishAnnotation:
 * @version:1.0
 * @param              {[type]} menu_id [description]
 * @return             {[type]}          [description]
 */
function delOperation(menu_id, api_delete_url) {
    menu_id = (menu_id == '' || menu_id == undefined || menu_id == 'undefined' || isEmpty(menu_id)) ? 0 : menu_id;
    if (!isNumber(menu_id) && menu_id <= 0) {
        layerMsg('菜单Id必须为数字类型！', 5, 1000);
        return false;
    }
    layer.confirm('确定要将菜单“' + $('tr#id_' + menu_id).attr('name') + '”进行“删除”操作吗？\b\b 删除之后将无法恢复！', {title: webTitle}, function () {
        axios.post( api_delete_url, {
            'menu_id': menu_id
        }).then(function(data){
            if (data.status == 1){
                layerMsg(data.msg, 1, layerJumpTime, 2);
            } else layerMsg(data.msg, 5, layerJumpTime);
        }).catch(function(res){
        });
    }, function () {
    });
}

function addMenus(url)
{
    hrefUrl(url + '?parent_id=' + $('form#form_data').find('select[name=parent_id]').val() || 0);
}