function delOperation(help_id, api_delete_url) {
    help_id = (help_id == '' || help_id == undefined || help_id == 'undefined' || isEmpty(help_id)) ? 0 : help_id;
    if (!isNumber(help_id) && help_id <= 0) {
        layerMsg('帮助中心Id必须为数字类型！', 5, layerJumpTime);
        return false;
    }
    layer.confirm('确定要将帮助中心“' + $('tr#id_' + help_id).attr('name') + '”进行“删除”操作吗？\b\b 删除之后将无法恢复！', {title: webTitle}, function () {
        axios.post( api_delete_url, {
            'help_id': help_id
        }).then(function(data){
            if (data.status == 1){
                layerMsg(data.msg, 1, layerJumpTime, 2);
            } else layerMsg(data.msg, 5, layerJumpTime);
        }).catch(function(res){
        });
    }, function () {
    });
}