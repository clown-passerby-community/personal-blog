function delOperation(log_id, api_delete_url, month) {
    log_id = (log_id == '' || log_id == undefined || log_id == 'undefined' || isEmpty(log_id)) ? 0 : log_id;
    month = (month == '' || month == undefined || month == 'undefined' || isEmpty(month)) ? '' : month;
    if (!isNumber(log_id) && log_id <= 0) {
        layerMsg('管理员登录日志的Id必须为数字类型！', 5, layerJumpTime);
        return false;
    }
    layer.confirm('确定要将管理员登录日志进行“删除”操作吗？\b\b 删除之后将无法恢复！', {title: webTitle}, function () {
        axios.post( api_delete_url, {
            'log_id': log_id,
            'month' : month
        }).then(function(data){
            if (data.status == 1){
                layerMsg(data.msg, 1, layerJumpTime, 2);
            } else layerMsg(data.msg, 5, layerJumpTime);
        }).catch(function(res){
        });
    }, function () {
    });
}