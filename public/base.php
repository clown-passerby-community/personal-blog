<?php
// 检测PHP环境
if(version_compare(PHP_VERSION,'7.1','<'))  die('PHP版本最低 7.1');

define('START_TIME', microtime(true));
define('START_MEM', memory_get_usage());

// 定义系统目录分隔符
define('DS', '/');

// 系统根目录
defined('ROOT_PATH') or define('ROOT_PATH', dirname(__FILE__).DS);

// 系统根目录 去除public
define('ROOT', str_replace('public'.DS, '', ROOT_PATH));

// 定义应用目录
defined('APP_PATH') or define('APP_PATH', ROOT.'app'.DS);

// 扩展类库目录
defined('EXTEND_PATH') or define('EXTEND_PATH', ROOT . 'extend' . DS);

// runtime目录
defined('RUNTIME_PATH') or define('RUNTIME_PATH', ROOT . 'runtime' . DS);

// runtime下面的log目录
defined('LOG_PATH') or define('LOG_PATH', RUNTIME_PATH . 'log' . DS);
defined('CACHE_PATH') or define('CACHE_PATH', RUNTIME_PATH . 'cache' . DS);
defined('TEMP_PATH') or define('TEMP_PATH', RUNTIME_PATH . 'temp' . DS);
defined('VENDOR_PATH') or define('VENDOR_PATH', ROOT . 'vendor' . DS);
defined('CONF_PATH') or define('CONF_PATH', ROOT . 'config' . DS); // 配置文件目录

// 环境常量
define('IS_CLI', PHP_SAPI == 'cli' ? true : false);
define('IS_WIN', strpos(PHP_OS, 'WIN') !== false);

// 数据库目录
defined('DATABSE_PATH') or define('DATABSE_PATH', ROOT.'database'.DS);
// 数据库迁移目录
define('MIGRATION_PATH', DATABSE_PATH.'migrations'.DS);

// public目录
defined('PUBLIC_PATH') or define('PUBLIC_PATH', ROOT . 'public' . DS);