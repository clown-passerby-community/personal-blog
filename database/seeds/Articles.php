<?php

use think\migration\Seeder;

class Articles extends Seeder
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        // faker默认语言是英文会生成英文的数据，在创建实例的时候可以指定为中文
        $faker = Faker\Factory::create('zh_CN');
        $rows = [];
        $time = time();
        for ($i = 0; $i < 1000; $i++) {
            $rows[] = [
                'menu_id'         => rand(1, 12),
                'article_title'      => $faker->word,
                'article_cover'      => rand(0, 2) == 1 ? '/static/blog.jpg' : '',
                'article_keywords'      => $faker->text(100),
                'article_description'      => $faker->text(200),
                'article_content'      => $faker->text(500),
                'read_num' => rand(0, 10000),
                'set_top' => rand(0, 1),
                'is_recommend' => rand(0, 1),
                'is_public' => 1,
                'article_sort' => rand(0, 1000),
                'created_time' => $time - rand(-10000, 10000),
                'updated_time' => $time,
            ];
        }

        \think\facade\Db::table(env('DATABASE.PREFIX') . 'articles')->insertAll($rows);
    }
}