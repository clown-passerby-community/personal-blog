<?php

use think\migration\Migrator;
use think\migration\db\Column;

class Articles extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        if ($this->hasTable('articles')) return;
        $this->table('articles', ['engine'=>'Innodb'])
             ->setComment('文章表')->setCollation(env('DATABASE.CHARSET_COLLATION'))
             ->setId('article_id')
             ->setPrimaryKey('article_id')
             ->addColumn(Column::integer('menu_id')->setUnsigned()->setDefault(0)->setComment('所属菜单'))
             ->addColumn('article_title', 'string', ['limit' => 200, 'default' => '', 'comment' => '标题'])
             ->addColumn('article_keywords', 'string', ['limit' => 200, 'default' => '', 'comment' => '关键词'])
             ->addColumn('article_description', 'string', ['limit' => 200, 'default' => '', 'comment' => '描述'])
             ->addColumn('article_cover', 'string', ['limit' => 200, 'default' => '', 'comment' => '封面'])
             ->addColumn(Column::text('article_content')->setComment('内容'))
             ->addColumn(Column::smallInteger('article_sort')->setUnsigned()->setDefault(0)->setComment('排序[升序]'))
             ->addColumn(Column::integer('read_num')->setUnsigned()->setDefault(0)->setComment('阅读量'))
             ->addColumn(Column::boolean('set_top')->setUnsigned()->setDefault(0)->setComment('是否置顶：0：否；1：是'))
             ->addColumn(Column::boolean('is_recommend')->setUnsigned()->setDefault(0)->setComment('是否推荐：0：否；1：是'))
             ->addColumn(Column::boolean('is_public')->setUnsigned()->setDefault(0)->setComment('是否公开：0：私密；1：是；2.密码访问'))
             ->addColumn('access_password', 'string', ['limit' => 60, 'default' => '', 'comment' => '访问密码'])
             ->addColumn(Column::boolean('is_delete')->setUnsigned()->setDefault(0)->setComment('是否删除：0：否；1：是'))
             ->addColumn(Column::integer('created_time')->setUnsigned()->setDefault(0)->setComment('创建时间'))
             ->addColumn(Column::integer('updated_time')->setUnsigned()->setDefault(0)->setComment('更新时间'))
            ->addColumn('article_origin', 'string', ['limit' => 200, 'default' => '', 'comment' => '文章来源'])
            ->addColumn('article_author', 'string', ['limit' => 200, 'default' => '', 'comment' => '文章作者'])
            ->addIndex('menu_id')
             ->addIndex('set_top')
             ->addIndex('is_recommend')
             ->addIndex('is_delete')
             ->create();
    }
}
