<?php

use think\migration\Migrator;
use think\migration\db\Column;

class AdminRoles extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        if ($this->hasTable('admin_roles')) return;
        $this->table('admin_roles', ['engine' => 'Innodb'])
             ->setComment('后台-角色表')
             ->setCollation(env('DATABASE.CHARSET_COLLATION'))
             ->setId('role_id')->setPrimaryKey('role_id')
            ->addColumn('role_name', 'string', ['limit' => 200, 'default' => '', 'comment' => '角色名称'])
            ->addColumn('role_remarks', 'string', ['limit' => 200, 'default' => '', 'comment' => '备注'])
            ->addColumn(Column::boolean('is_check')->setUnsigned()->setDefault(1)->setComment('是否可用：1：可用；0：禁用'))
            ->addColumn(Column::boolean('is_delete')->setUnsigned()->setDefault(0)->setComment('是否删除：1：删除；0：正常'))
            ->addColumn(Column::integer('created_time')->setUnsigned()->setDefault(0)->setComment('创建时间'))
            ->addColumn(Column::integer('updated_time')->setUnsigned()->setDefault(0)->setComment('更新时间'))
            ->addIndex('is_check')
            ->addIndex('is_delete')
            ->create();
    }

    /**
     * Migrate Up.
     */
    public function up()
    {

    }
    
    /**
     * Migrate Down.
     */
    public function down()
    {
        $this->dropTable('admin_roles');
    }
}
