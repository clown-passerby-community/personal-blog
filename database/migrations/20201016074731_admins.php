<?php

use think\migration\Migrator;
use think\migration\db\Column;

class Admins extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        if ($this->hasTable('admins')) return;
        $this->table('admins', ['engine' => 'Innodb'])
             ->setComment('管理员表')
             ->setCollation(env('DATABASE.CHARSET_COLLATION'))
             ->setId('admin_id')->setPrimaryKey('admin_id')
             ->addColumn('admin_name', 'string', ['limit' => 100, 'default' => '', 'comment' => '用户名'])
             ->addColumn('admin_email', 'string', ['limit' => 100, 'default' => '', 'comment' => '邮箱'])
             ->addColumn('admin_head', 'string', ['limit' => 200, 'default' => '', 'comment' => '头像'])
             ->addColumn('password', 'string', ['limit' => 60, 'default' => '', 'comment' => '密码'])
             ->addColumn(Column::boolean('is_check')->setUnsigned()->setDefault(1)->setComment('登陆状态[0.尚未开放；1.正常；2.禁用]'))
             ->addColumn(Column::boolean('kick_out')->setUnsigned()->setDefault(2)->setComment('是否踢出登录[0：表示在线；1：踢出登录；2.未登录]'))
             ->addColumn(Column::boolean('is_delete')->setUnsigned()->setDefault(0)->setComment('是否删除：1：是；0：否'))
             ->addIndex('is_check')
             ->addIndex('is_delete')
             ->create();
    }

    /**
     * Migrate Up.
     */
    public function up()
    {

    }
    
    /**
     * Migrate Down.
     */
    public function down()
    {
        $this->dropTable('admins');
    }
}
