<?php

use think\migration\Migrator;
use think\migration\db\Column;

class Banners extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        if ($this->hasTable('banners')) return;
        $this->table('banners', ['engine'=>'Innodb'])
             ->setComment('Banner表')->setCollation(env('DATABASE.CHARSET_COLLATION'))
             ->setId('banner_id')
             ->setPrimaryKey('banner_id')
             ->addColumn('banner_title', 'string', ['limit' => 200, 'default' => '', 'comment' => '标题'])
             ->addColumn('banner_cover', 'string', ['limit' => 200, 'default' => '', 'comment' => '封面'])
             ->addColumn('banner_link', 'string', ['limit' => 200, 'default' => '', 'comment' => '外链'])
             ->addColumn('banner_words', 'string', ['limit' => 200, 'default' => '', 'comment' => '文字描述'])
             ->addColumn(Column::smallInteger('banner_sort')->setUnsigned()->setDefault(0)->setComment('排序[升序]'))
             ->addColumn(Column::boolean('is_check')->setUnsigned()->setDefault(0)->setComment('是否审核：0：禁用；1.启用'))
             ->addColumn(Column::boolean('is_delete')->setUnsigned()->setDefault(0)->setComment('是否删除：0：否；1：是'))
             ->addColumn(Column::integer('created_time')->setUnsigned()->setDefault(0)->setComment('创建时间'))
             ->addColumn(Column::integer('updated_time')->setUnsigned()->setDefault(0)->setComment('更新时间'))
             ->addIndex('banner_sort')
             ->addIndex('is_check')
             ->addIndex('is_delete')
             ->create();
    }
}