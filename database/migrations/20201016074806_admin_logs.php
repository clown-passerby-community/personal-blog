<?php

use think\migration\Migrator;
use think\migration\db\Column;

class AdminLogs extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        if ($this->hasTable('admin_logs')) return;
        // 【按月分表】
        // 作为基础表，所有的分表生成按照词表的结构进行复制并重命名
        $this->table('admin_logs', ['engine' => 'Innodb'])
             ->setComment('管理员操作日志表')
             ->setCollation(env('DATABASE.CHARSET_COLLATION'))
             ->setId('log_id')
             ->setPrimaryKey('log_id')
             ->addColumn(Column::integer('admin_id')->setUnsigned()->setDefault(0)->setComment('管理员Id'))
             ->addColumn(Column::string('created_ip', 20)->setDefault('')->setComment('创建时的IP'))
             ->addColumn(Column::string('browser_type', 256)->setDefault('')->setComment('创建时浏览器类型'))
             ->addColumn(Column::boolean('log_status')->setUnsigned()->setDefault(1)->setComment('状态：1：成功；0：失败'))
             ->addColumn(Column::string('description', 256)->setDefault('')->setComment('描述'))
             ->addColumn(Column::string('log_action', 100)->setDefault('')->setComment('请求方法'))
             ->addColumn(Column::string('log_method', 20)->setDefault('')->setComment('请求类型/请求方式'))
             ->addColumn(Column::text('request_data')->setComment('请求参数'))
             ->addColumn(Column::boolean('is_delete')->setUnsigned()->setDefault(0)->setComment('是否删除：1：是；0：否'))
             ->addColumn(Column::integer('created_time')->setUnsigned()->setDefault(0)->setComment('创建时间'))
             ->addColumn(Column::integer('updated_time')->setUnsigned()->setDefault(0)->setComment('更新时间'))
             ->addIndex('admin_id')
             ->addIndex('log_status')
             ->addIndex('is_delete')
             ->create();
    }

    /**
     * Migrate Up.
     */
    public function up()
    {

    }
    
    /**
     * Migrate Down.
     */
    public function down()
    {
        $this->dropTable('admin_logs');
    }
}
