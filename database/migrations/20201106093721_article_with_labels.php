<?php

use think\migration\Migrator;
use think\migration\db\Column;

class ArticleWithLabels extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        if ($this->hasTable('article_with_labels')) return;
        $this->table('article_with_labels', ['engine'=>'Innodb'])
            ->setComment('文章标签关联表')->setCollation(env('DATABASE.CHARSET_COLLATION'))
            ->setId('id')
            ->setPrimaryKey('id')
            ->addColumn(Column::integer('article_id')->setUnsigned()->setDefault(0)->setComment('文章Id'))
            ->addColumn(Column::integer('label_id')->setUnsigned()->setDefault(0)->setComment('标签Id'))
            ->addIndex('article_id')
            ->addIndex('label_id')
            ->create();
    }
}
