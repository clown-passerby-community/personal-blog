<?php

use think\migration\Migrator;
use think\migration\db\Column;

class AdminRoleWithMenus extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        if ($this->hasTable('admin_role_with_menus')) return;
        $this->table('admin_role_with_menus', ['engine' => 'Innodb'])
             ->setComment('后台-角色与菜单关联表')
             ->setCollation(env('DATABASE.CHARSET_COLLATION'))
             ->setId('with_id')->setPrimaryKey('with_id')
             ->addColumn(Column::integer('role_id')->setUnsigned()->setDefault(0)->setComment('角色Id'))
             ->addColumn(Column::integer('menu_id')->setUnsigned()->setDefault(0)->setComment('菜单Id'))
             ->addIndex('role_id')
             ->addIndex('menu_id')
             ->create();
    }

    /**
     * Migrate Up.
     */
    public function up()
    {

    }
    
    /**
     * Migrate Down.
     */
    public function down()
    {
        $this->dropTable('admin_role_with_menus');
    }
}
