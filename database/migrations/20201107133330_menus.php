<?php

use think\migration\Migrator;
use think\migration\db\Column;

class Menus extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        if ($this->hasTable('menus')) return;
        $this->table('menus', ['engine' => 'Innodb'])
            ->setComment('前台菜单表')
            ->setCollation(env('DATABASE.CHARSET_COLLATION'))
            ->setId('menu_id')
            ->setPrimaryKey('menu_id')
            ->addColumn(Column::integer('parent_id')->setUnsigned()->setDefault(0)->setComment('父级id'))
            ->addColumn('menu_name', 'string', ['limit' => 200, 'default' => '', 'comment' => '菜单名称'])
            ->addColumn('menu_icon', 'string', ['limit' => 200, 'default' => '', 'comment' => '图标'])
            ->addColumn(Column::integer('menu_tpltype')->setUnsigned()->setDefault(0)->setComment('模板类型'))
            ->addColumn('menu_list', 'string', ['limit' => 200, 'default' => '', 'comment' => '列表页模板'])
            ->addColumn('menu_detail', 'string', ['limit' => 200, 'default' => '', 'comment' => '详情模板'])
            ->addColumn('menu_url', 'string', ['limit' => 200, 'default' => '', 'comment' => '链接'])
            ->addColumn('menu_cover', 'string', ['limit' => 200, 'default' => '', 'comment' => '封面/缩略图'])
            ->addColumn('menu_bigimg', 'string', ['limit' => 200, 'default' => '', 'comment' => '大封面图'])
            ->addColumn('menu_keywords', 'string', ['limit' => 100, 'default' => '', 'comment' => 'head头部展示的关键字搜索'])
            ->addColumn('menu_description', 'string', ['limit' => 100, 'default' => '', 'comment' => 'head头部展示的描述'])
            ->addColumn(Column::text('menu_content')->setComment('内容'))
            ->addColumn(Column::integer('menu_sort')->setUnsigned()->setDefault(0)->setComment('排序[从小到大]'))
            ->addColumn(Column::boolean('is_show')->setUnsigned()->setDefault(0)->setComment('是否展示在首页：1：展示；0：隐藏'))
            ->addColumn(Column::boolean('is_delete')->setUnsigned()->setDefault(0)->setComment('是否删除：1：删除；0：正常'))
            ->addColumn(Column::integer('created_time')->setUnsigned()->setDefault(0)->setComment('创建时间'))
            ->addColumn(Column::integer('updated_time')->setUnsigned()->setDefault(0)->setComment('更新时间'))
            ->addIndex('parent_id')
            ->addIndex('is_show')
            ->addIndex('is_delete')
            ->create();
    }
}
