<?php

use think\migration\Migrator;
use think\migration\db\Column;

class Notices extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        if ($this->hasTable('notices')) return;
        // 【按月分表】
        // 作为基础表，所有的分表生成按照词表的结构进行复制并重命名
        $this->table('notices', ['engine'=>'Innodb'])
             ->setComment('系统公告表')
             ->setCollation(env('DATABASE.CHARSET_COLLATION'))
             ->setId('notice_id')
             ->setPrimaryKey('notice_id')
             ->addColumn(Column::boolean('notice_type')->setUnsigned()->setDefault(0)->setComment('公告类型：0.系统通知（所有人）'))
             ->addColumn('notice_title', 'string', ['limit' => 200, 'default' => '', 'comment' => '公告标题'])
             ->addColumn(Column::text('notice_content')->setComment('内容'))
             ->addColumn(Column::integer('admin_id')->setUnsigned()->setDefault(0)->setComment('管理员Id'))
             ->addColumn(Column::string('created_ip', 20)->setDefault('')->setComment('创建时的IP'))
             ->addColumn(Column::integer('created_time')->setUnsigned()->setDefault(0)->setComment('创建时间'))
             ->addColumn(Column::integer('updated_time')->setUnsigned()->setDefault(0)->setComment('更新时间'))
             ->addIndex('notice_type')
             ->create();
    }
}
