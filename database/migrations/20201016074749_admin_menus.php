<?php

use think\migration\Migrator;
use think\migration\db\Column;

class AdminMenus extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        if ($this->hasTable('admin_menus')) return;
        $this->table('admin_menus', ['engine' => 'Innodb'])
             ->setComment('后台菜单表')
             ->setCollation(env('DATABASE.CHARSET_COLLATION'))
             ->setId('menu_id')
             ->setPrimaryKey('menu_id')
             ->addColumn(Column::integer('parent_id')->setUnsigned()->setDefault(0)->setComment('父级id'))
             ->addColumn('menu_name', 'string', ['limit' => 256, 'default' => '', 'comment' => '菜单名称'])
             ->addColumn('menu_url', 'string', ['limit' => 256, 'default' => '', 'comment' => '控制器/方法'])
             ->addColumn('menu_icon', 'string', ['limit' => 100, 'default' => '', 'comment' => '图标'])
             ->addColumn(Column::integer('menu_sort')->setUnsigned()->setDefault(0)->setComment('排序[从小到大]'))
             ->addColumn(Column::boolean('is_left')->setUnsigned()->setDefault(0)->setComment('是否展示左侧：1：展示；0：隐藏'))
             ->addColumn(Column::boolean('is_check')->setUnsigned()->setDefault(1)->setComment('是否可用：1：可用；0：禁用'))
             ->addColumn(Column::boolean('is_delete')->setUnsigned()->setDefault(0)->setComment('是否删除：1：删除；0：正常'))
             ->addColumn(Column::integer('created_time')->setUnsigned()->setDefault(0)->setComment('创建时间'))
             ->addColumn(Column::integer('updated_time')->setUnsigned()->setDefault(0)->setComment('更新时间'))
             ->addIndex('parent_id')
             ->addIndex('is_left')
             ->addIndex('is_check')
             ->addIndex('is_delete')
             ->create();
    }

    /**
     * Migrate Up.
     */
    public function up()
    {

    }
    
    /**
     * Migrate Down.
     */
    public function down()
    {
        $this->dropTable('admin_menus');
    }
}
