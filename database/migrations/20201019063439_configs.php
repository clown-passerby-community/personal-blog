<?php

use think\migration\Migrator;
use think\migration\db\Column;

class Configs extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        if ($this->hasTable('configs')) return;
        $this->table('configs', ['engine'=>'Innodb'])
             ->setComment('网站配置表')->setCollation(env('DATABASE.CHARSET_COLLATION'))
             ->setId('config_id')
             ->setPrimaryKey('config_id')
             ->addColumn('config_title', 'string', ['limit' => 200, 'default' => '', 'comment' => '标题'])
             ->addColumn('config_name', 'string', ['limit' => 200, 'default' => '', 'comment' => '配置名称'])
             ->addColumn(Column::text('config_value')->setComment('配置值'))
             ->addColumn('config_extra', 'string', ['limit' => 200, 'default' => '', 'comment' => '配置项'])
             ->addColumn('config_remarks', 'string', ['limit' => 200, 'default' => '', 'comment' => '说明'])
             ->addColumn(Column::smallInteger('config_group')->setUnsigned()->setDefault(0)->setComment('配置分组'))
             ->addColumn(Column::smallInteger('config_type')->setUnsigned()->setDefault(0)->setComment('配置类型'))
             ->addColumn(Column::smallInteger('config_sort')->setUnsigned()->setDefault(0)->setComment('排序[从小到大]'))
             ->addColumn(Column::boolean('is_check')->setUnsigned()->setDefault(1)->setComment('是否启用：1：是；0：否'))
             ->addColumn(Column::boolean('is_delete')->setUnsigned()->setDefault(0)->setComment('是否删除：1：是；0：否'))
             ->addColumn(Column::integer('created_time')->setUnsigned()->setDefault(0)->setComment('创建时间'))
             ->addColumn(Column::integer('updated_time')->setUnsigned()->setDefault(0)->setComment('更新时间'))
             ->addIndex('config_group')
             ->addIndex('is_delete')
             ->create();
    }

    /**
     * Migrate Up.
     */
    public function up()
    {

    }
    
    /**
     * Migrate Down.
     */
    public function down()
    {
        $this->dropTable('configs');
    }
}
