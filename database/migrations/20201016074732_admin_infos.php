<?php

use think\migration\Migrator;
use think\migration\db\Column;

class AdminInfos extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        if ($this->hasTable('admin_infos')) return;
        $this->table('admin_infos', ['engine' => 'Innodb'])
             ->setComment('管理员信息表')
             ->setCollation(env('DATABASE.CHARSET_COLLATION'))
             ->setId(false)
             ->addColumn(Column::integer('admin_id')->setUnsigned()->setDefault(0)->setComment('管理员Id'))
             ->addColumn(Column::integer('login_num')->setUnsigned()->setDefault(0)->setComment('登录次数'))
             ->addColumn('created_ip', 'string', ['limit' => 20, 'default' => '', 'comment' => '创建时的IP'])
             ->addColumn('browser_type', 'string', ['limit' => 200, 'default' => '', 'comment' => '创建时浏览器类型'])
             ->addColumn(Column::integer('created_time')->setUnsigned()->setDefault(0)->setComment('创建时间'))
             ->addColumn(Column::integer('updated_time')->setUnsigned()->setDefault(0)->setComment('更新时间'))
             ->addIndex('admin_id')
             ->create();
    }

    /**
     * Migrate Up.
     */
    public function up()
    {

    }
    
    /**
     * Migrate Down.
     */
    public function down()
    {
        $this->dropTable('admin_infos');
    }
}
