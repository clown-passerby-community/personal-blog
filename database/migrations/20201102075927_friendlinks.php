<?php

use think\migration\Migrator;
use think\migration\db\Column;

class Friendlinks extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        if ($this->hasTable('friendlinks')) return;
        $this->table('friendlinks', ['engine'=>'Innodb'])
             ->setComment('友情外链表')->setCollation(env('DATABASE.CHARSET_COLLATION'))
             ->setId('link_id')
             ->setPrimaryKey('link_id')
             ->addColumn('link_name', 'string', ['limit' => 200, 'default' => '', 'comment' => '外链名称'])
             ->addColumn('link_url', 'string', ['limit' => 200, 'default' => '', 'comment' => '外链URL'])
             ->addColumn('link_cover', 'string', ['limit' => 200, 'default' => '', 'comment' => '封面'])
             ->addColumn('link_words', 'string', ['limit' => 200, 'default' => '', 'comment' => '文字描述'])
             ->addColumn(Column::smallInteger('link_sort')->setUnsigned()->setDefault(0)->setComment('排序[升序]'))
             ->addColumn(Column::boolean('is_check')->setUnsigned()->setDefault(0)->setComment('是否审核：0：禁用；1.启用'))
             ->addColumn(Column::boolean('is_delete')->setUnsigned()->setDefault(0)->setComment('是否删除：0：否；1：是'))
             ->addColumn(Column::integer('created_time')->setUnsigned()->setDefault(0)->setComment('创建时间'))
             ->addColumn(Column::integer('updated_time')->setUnsigned()->setDefault(0)->setComment('更新时间'))
             ->addIndex('link_sort')
             ->addIndex('is_check')
             ->addIndex('is_delete')
             ->create();
    }
}
