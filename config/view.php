<?php
// +----------------------------------------------------------------------
// | 模板设置
// +----------------------------------------------------------------------

return [
    // 模板引擎类型使用Think
    'type'          => 'Think',
    // 默认模板渲染规则 1 解析为小写+下划线 2 全部转换小写 3 保持操作方法
    'auto_rule'     => 1,
    // 模板目录名
    'view_dir_name' => 'view',
    // 模板后缀
    'view_suffix'   => 'html',
    // 模板文件名分隔符
    'view_depr'     => DIRECTORY_SEPARATOR,
    // 模板引擎普通标签开始标记
    'tpl_begin'     => '{',
    // 模板引擎普通标签结束标记
    'tpl_end'       => '}',
    // 标签库标签开始标记
    'taglib_begin'  => '{',
    // 标签库标签结束标记
    'taglib_end'    => '}',
    // 静态资源文件字符串替换
    'tpl_replace_string' => [
        '__STATIC__' => env('url') . '/static',
        '__ACE__' => '/static/ace',
        '__LIGHT_YEAR__' => '/static/light-year-admin',
        '__LAYER__' => '/static/layer',
        '__LAYUI__' => '/static/layui',
        '__ADMIN__' => '/static/cnpscy_admin',
        '__WEBUPLOADER__' => '/static/webuploader-0.1.5',
        '__EDITOR_MD__' => '/static/editor.md',
        '__HOME__' => '/static/home',



        '__STATIC_ADMIN__' => env('url') . '/static/admin',
        '__JS__' => env('url') . '/static/js',
        '__PLUGINS__' => env('url') . '/static/plugins',
    ],
];
