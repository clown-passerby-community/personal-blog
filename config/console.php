<?php
// +----------------------------------------------------------------------
// | 控制台配置
// +----------------------------------------------------------------------
return [
    // 指令定义
    'commands' => [
        'autotablebuild' => \app\command\AutoTableBuild::class,
        'checkuserordertakingstatus' => \app\command\CheckUserOrdertakingStatus::class,
    ],
];
