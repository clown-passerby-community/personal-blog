<?php
namespace app\home\controller;

use app\common\model\Article\ArticleLabels;
use app\home\model\Articles;
use app\home\model\Menus;
use app\common\model\System\Friendlinks;
use think\App;
use think\facade\View;

class Indexs extends Base
{
    public function __construct(App $app)
    {
        parent::__construct($app);
        // 菜单栏目
        $menus = Menus::getInstance()->getWebList();
        View::assign('menus', $menus);
        // 所有的标签
        $labels = ArticleLabels::getInstance()->deleteWhere()->select();
        View::assign('labels', $labels);
        // 所有的友情链接
        $friendlinks = Friendlinks::getInstance()->deleteWhere()->select();
        View::assign('friendlinks', $friendlinks);

        $articles_model = Articles::getInstance();
        // 置顶文章
        $article_settops = $articles_model->deleteWhere()
            ->where('is_public', 1)
            ->where('set_top', 1)
            ->order('article_sort', 'ASC')
            ->order('read_num', 'DESC')
            ->limit(15)
            ->select();
        View::assign('article_settops', $article_settops);
        // 推荐文章
        $article_recommends = $articles_model->deleteWhere()
            ->where('is_public', 1)
            ->where('is_recommend', 1)
            ->order('article_sort', 'ASC')
            ->order('read_num', 'DESC')
            ->limit(15)
            ->select();
        View::assign('article_recommends', $article_recommends);
    }

    public function index(string $menu = '')
    {
        if (empty($menu)){
            // 热门文章
            $articles_model = Articles::getInstance();
            $hot_articles = $articles_model->deleteWhere()
                ->where('is_public', 1)
                ->order('read_num', 'DESC')
                ->order('article_sort', 'ASC')
                ->limit(8)
                ->select();

            // 最新发布
            $new_articles = $articles_model->deleteWhere()
                ->where('is_public', 1)
                ->with(['menu', 'labels'])
                ->order('created_time', 'DESC')
                ->limit(10)
                ->paginate();

            View::assign('web_title', '首页');
            View::assign('web_keywords', cnpscy_config('site_web_keywords'));
            View::assign('web_description', cnpscy_config('site_web_description'));
            return view(__FUNCTION__, compact('hot_articles', 'new_articles'));
        }else{
            return $this->template($menu);
        }
    }

    public function template(string $menu = '')
    {
        if (empty($menu)) {
            return $this->redirect(url('/'));
        }
        $menus = Menus::getInstance();
        $detail = $menus->getDetailByIdOrUrl($menu);
        if (empty($detail)){
            return $this->redirect(url('/'));
        }
        $menu_ids = $menus->getChildIds($detail->menu_id);
        $articles = Articles::getInstance()
            ->deleteWhere()
            ->where('menu_id', 'IN', $menu_ids)
            ->where('is_public', 1)
            ->with(['menu', 'labels'])
            ->order('set_top', 'DESC')
            ->order('is_recommend', 'DESC')
            ->order('article_sort', 'ASC')
            ->paginate(15);

        View::assign('web_title', $detail->menu_name ?? cnpscy_config('site_web_title'));
        View::assign('web_keywords', empty($detail->menu_keywords) ? cnpscy_config('site_web_keywords') : $detail->menu_keywords);
        View::assign('web_description', empty($detail->menu_description) ? cnpscy_config('site_web_description') : $detail->menu_description);
        return view($detail->menu_list, compact('detail', 'articles'));
    }

    public function detail(Articles $articles, int $article_id)
    {
        $detail = $articles->detail($article_id, '', false, ['menu', 'labels']);

        View::assign('web_title', $detail->article_title ?? cnpscy_config('site_web_title'));
        View::assign('web_keywords', empty($detail->article_keywords) ? cnpscy_config('site_web_keywords') : $detail->article_keywords);
        View::assign('web_description', empty($detail->article_description) ? cnpscy_config('site_web_description') : $detail->article_description);

        return view('', compact('detail'));
    }

    public function search(string $keywords = '')
    {
        $articles = Articles::getInstance()
            ->deleteWhere()
            ->where('article_title|article_keywords', 'LIKE', $keywords . '%')
            ->where('is_public', 1)
            ->with(['menu', 'labels'])
            ->order('set_top', 'DESC')
            ->order('is_recommend', 'DESC')
            ->order('article_sort', 'ASC')
            ->paginate(15);

        $detail = (object)['menu_name' => $keywords];

        View::assign('web_title', $detail->menu_name ?? cnpscy_config('site_web_title'));
        View::assign('web_keywords', cnpscy_config('site_web_keywords'));
        View::assign('web_description', cnpscy_config('site_web_description'));
        return view('articles', compact('detail', 'articles'));
    }
}