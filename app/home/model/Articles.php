<?php
declare (strict_types = 1);

namespace app\home\model;

class Articles extends \app\common\model\Article\Articles
{
    public function menu()
    {
        return $this->belongsTo(Menus::class, 'menu_id', 'menu_id');
    }
}
