<?php
declare (strict_types = 1);

namespace app\common\validate;

use think\Validate;

class BaseValidate extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名' =>  ['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [];

    /**
     * 定义错误信息
     * 格式：'字段名.规则名' =>  '错误信息'
     *
     * @var array
     */
    protected $message = [];

    public function getRules()
    {
        return $this->rule;
    }

    public function getMessages()
    {
        return $this->message;
    }

    public function __construct()
    {
        parent::__construct();

        // 如果存在设置rule方法，那么自动调用
        if (method_exists($this, 'setRules')){
            $this->rule = $this->setRules();
        }
    }
}
