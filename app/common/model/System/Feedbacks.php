<?php
declare (strict_types = 1);

namespace app\common\model\System;

use app\common\model\Common;

class Feedbacks extends Common
{
    // 主键
    protected $pk = 'feedback_id';
    // 开启自动写入时间戳字段
    protected $autoWriteTimestamp = true;
}
