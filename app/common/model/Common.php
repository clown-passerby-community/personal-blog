<?php

namespace app\common\model;

use app\Traits\AdminBaseFunctionsModel;
use app\Traits\Instance;
use app\Traits\MysqlTable;
use think\Model;

class Common extends Model
{
    // 单例
    use Instance;

    use MysqlTable;

    use AdminBaseFunctionsModel;

    // 主键
    protected $pk = 'id';
    // 开启自动写入时间戳字段
    protected $autoWriteTimestamp = false;
    /**
     * 创建时间字段 false表示关闭
     *
     * @var false|string
     */
    protected $createTime = 'created_time';
    /**
     * 更新时间字段 false表示关闭
     *
     * @var false|string
     */
    protected $updateTime = 'updated_time';
    /**
     * 自定义的软删除
     */
    public $is_delete = 1; //是否开启删除（1.开启删除，就是直接删除；0.假删除）
    public $delete_field = 'is_delete'; //删除字段




    // 定义全局的查询范围（有坑……，很多时候无效果）
    protected $globalScope = ['delete'];
    private $load_global_scope = false;
    public function scopeDelete($query)
    {
        // 不可删除下面两行代码；删除……系统直接502/500
        if ($this->load_global_scope == true) return $query;
        $this->load_global_scope = true;

        if ( intval($this->is_delete) == 0 && !empty($this->delete_field) ) return $query->where($this->getTable() . '.' . $this->delete_field, 0);
        return $query;
    }
}