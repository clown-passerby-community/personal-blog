<?php
declare (strict_types = 1);

namespace app\common\model;

class HelpCenters extends Common
{
    // 主键
    protected $pk = 'help_id';
    // 开启自动写入时间戳字段
    protected $autoWriteTimestamp = true;
    // 自定义的软删除
    public $is_delete = 0; // 软删除

    public function getHelpContentAttr($value)
    {
        return html_entity_decode(htmlspecialchars_decode($value));
    }

    public function setHelpContentAttr($value)
    {
        return htmlspecialchars($value);
    }
}
