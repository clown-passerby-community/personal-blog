<?php
declare (strict_types = 1);

namespace app\common\model\Article;

use app\common\model\Common;
use app\common\model\Menus;

class Articles extends Common
{
    // 主键
    protected $pk = 'article_id';
    // 开启自动写入时间戳字段
    protected $autoWriteTimestamp = true;
    // 自定义的软删除
    public $is_delete = 0; // 软删除

    public function category()
    {
        return $this->hasOne(ArticleCategories::class, 'category_id', 'category_id');
    }

    public function labels()
    {
        return $this->hasManyThrough(ArticleLabels::class, ArticleWithLabels::class, $this->pk, 'label_id', $this->pk, 'label_id');
    }

    public function menu()
    {
        return $this->belongsTo(Menus::class, 'menu_id', 'menu_id');
    }
}