<?php
declare (strict_types = 1);

namespace app\common\model;

class PaymentCallbacks extends Common
{
    // 主键
    protected $pk = 'callback_id';
    // 开启自动写入时间戳字段
    protected $autoWriteTimestamp = true;
    // 自定义的软删除
    public $is_delete = 0; // 软删除
}
