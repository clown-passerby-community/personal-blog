<?php
declare (strict_types = 1);

namespace app\common\model\Rabc;

use app\common\model\MonthTable;

class AdminLoginLogs extends MonthTable
{
    // 主键
    protected $pk = 'log_id';
    // 开启自动写入时间戳字段
    protected $autoWriteTimestamp = true;
    // 自定义的软删除
    public $is_delete = 0; // 软删除

    public function admin()
    {
        return $this->belongsTo(Admins::class, 'admin_id');
    }

    public function record($admin, int $log_status = 1, string $description = '登录成功！')
    {
        $client_info = get_client_info();
        return $this->add([
            'admin_id' => $admin->admin_id ?? 0,
            'created_ip' => $client_info['ip'],
            'browser_type' => $client_info['agent'],
            'log_status' => $log_status,
            'description' => $description,
            'log_action' => request()->url(),
            'log_method' => request()->method(),
            'request_data' => my_json_encode(input()),
        ]);
    }

    public function getRecentLists(int $admin_id)
    {
        $list = $this->deleteWhere()->with('admin')->limit(10)->order('created_time', 'DESC')->select();
        return $list;
    }
}
