<?php
declare (strict_types = 1);

namespace app\common\model\Rabc;

use app\common\model\Common;

class AdminMenus extends Common
{
    // 主键
    protected $pk = 'menu_id';
    // 开启自动写入时间戳字段
    protected $autoWriteTimestamp = true;
    // 自定义的软删除
    public $is_delete = 0; // 软删除

    public function getSelectLists($where = ['is_check' => 1], $sort = 'ASC', string $fileds = '*')
    {
        if (empty($where)) $where = ['is_check' => 1];
        $list = parent::getSelectLists($where, $sort, $fileds);
        return list_to_tree($list->toArray());
    }
}
