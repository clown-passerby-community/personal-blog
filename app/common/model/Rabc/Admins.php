<?php
declare (strict_types = 1);

namespace app\common\model\Rabc;

use app\common\model\Common;

class Admins extends Common
{
    // 主键
    protected $pk = 'admin_id';
    // 自定义的软删除
    public $is_delete = 0; // 软删除

    public function adminInfo()
    {
        return $this->hasOne(AdminInfos::class, $this->pk, $this->pk);
    }

    public function role()
    {
        return $this->hasOneThrough(AdminRoles::class, AdminWithRoles::class, 'admin_id', 'role_id', 'admin_id', 'role_id');
    }

    public function getAdminByName(string $admin_name)
    {
        return $this->where('admin_name', $admin_name)->find();
    }
}
