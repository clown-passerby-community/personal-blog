<?php
declare (strict_types = 1);

namespace app\common\model\Rabc;

use app\common\model\Common;

class AdminRoles extends Common
{
    // 主键
    protected $pk = 'role_id';
    // 开启自动写入时间戳字段
    protected $autoWriteTimestamp = true;
    // 自定义的软删除
    public $is_delete = 0; // 软删除

    public function getSelectLists($where = [], $sort = 'ASC', string $fileds = '*')
    {
        return parent::getSelectLists(['is_check' => 1], $sort, $fileds);
    }

    public function menus()
    {
        return $this->hasManyThrough(AdminMenus::class, AdminRoleWithMenus::class, 'role_id', 'menu_id', 'role_id', 'menu_id');
    }
}
