<?php
declare (strict_types = 1);

namespace app\common\model\Rabc;

use app\common\model\MonthTable;

class AdminLogs extends MonthTable
{
    // 主键
    protected $pk = 'log_id';
    // 开启自动写入时间戳字段
    protected $autoWriteTimestamp = true;
    // 自定义的软删除
    public $is_delete = 0; // 软删除

    public function admin()
    {
        return $this->belongsTo(Admins::class, 'admin_id');
    }

    public function record(int $admin_id = 0, string $method = 'POST', string $description = '')
    {
        $client_info = get_client_info();
        return $this->add([
            'admin_id' => $admin_id,
            'created_ip' => $client_info['ip'],
            'browser_type' => $client_info['agent'],
            'description' => empty($description) ? '' : $description,
            'log_action' => request()->url(),
            'log_method' => $method,
            'request_data' => my_json_encode(input()),
        ]);
    }
}
