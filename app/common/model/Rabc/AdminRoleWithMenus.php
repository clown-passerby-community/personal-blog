<?php
declare (strict_types = 1);

namespace app\common\model\Rabc;

use app\common\model\Common;

class AdminRoleWithMenus extends Common
{
    // 主键
    protected $pk = 'with_id';
}
