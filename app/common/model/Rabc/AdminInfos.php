<?php
declare (strict_types = 1);

namespace app\common\model\Rabc;

use app\common\model\Common;

class AdminInfos extends Common
{
    // 主键
    protected $pk = 'admin_id';
    // 开启自动写入时间戳字段
    protected $autoWriteTimestamp = true;
}
