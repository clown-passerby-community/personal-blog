<?php
/**
 * Created by IntelliJ IDEA.
 * User: pc
 * Date: 2020/10/19
 * Time: 11:36
 * Note: Service.php
 */

namespace app\common\services;

use app\Traits\Error;
use app\Traits\Instance;

abstract class Service
{
    use Error;

    use Instance;
}