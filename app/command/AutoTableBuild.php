<?php
declare (strict_types = 1);

namespace app\command;

use app\common\model\Rabc\AdminLoginLogs;
use app\common\model\Rabc\AdminLogs;
use think\console\Command;
use think\console\Input;
use think\console\input\Argument;
use think\console\input\Option;
use think\console\Output;

/**
 * Class AutoTableBuild
 *
 * 按月自动分表
 *
 * @package app\command
 */
class AutoTableBuild extends Command
{
    // 自动创建按月分表的定义
    public static $tabel_list = [
        // 管理员登录记录
        AdminLoginLogs::class,
        // 管理员日志
        AdminLogs::class,
    ];

    protected function configure()
    {
        // 指令配置
        $this->setName('autotablebuild')
            ->setDescription('the autotablebuild command');
    }

    protected function execute(Input $input, Output $output)
    {
        $this->startExecute();

        // 指令输出
        $output->writeln('autotablebuild');
    }

    private function startExecute()
    {
        if (!empty(self::$tabel_list)){
            foreach (self::$tabel_list as $table){
                (new $table)->createMonthTable('', strtotime('+1 month'));
            }
        }
    }
}
