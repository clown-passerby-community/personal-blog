<?php

namespace app\admin\services;

use app\admin\model\Admins;
use app\common\model\Rabc\AdminLoginLogs;
use app\common\services\Service;

class IndexService extends Service
{
    public function indexs(int $admin_id)
    {
        // 获取登录会员账户最近登录的10次记录
        $admin_login_logs = AdminLoginLogs::getInstance()->getRecentLists($admin_id);

        return [
            'admin_login_logs' => $admin_login_logs,
        ];
    }

    /**
     * 登录会员的个人资料编辑
     *
     * @param  int    $admin_id
     * @param  array  $params
     *
     * @return bool
     */
    public function updateAdmin(int $admin_id, array $params): bool
    {
        $admin = Admins::getInstance()->detail($admin_id);

        if ($admin->where('admin_id', $admin->admin_id)->update(['admin_email' => $params['admin_email'], 'admin_head' => $params['admin_head']])){
            $this->setError('个人资料编辑成功！');
            return true;
        }else{
            $this->setError('个人资料编辑失败！');
            return false;
        }
    }

    /**
     * 修改登录会员的密码
     *
     * @param  int    $admin_id
     * @param  array  $params
     *
     * @return bool
     */
    public function setLoginAdminPassword(int $admin_id, array $params):bool
    {
        $admin = Admins::getInstance()->detail($admin_id);
        if (!hash_verify($params['old_password'], $admin->password)){
            $this->setError('原始密码输入错误！');
            return false;
        }

        if ($admin->where('admin_id', $admin->admin_id)->update(['password' => hash_make($params['password'])])){
            $this->setError('密码修改成功！');
            return true;
        }else{
            $this->setError('密码修改失败！');
            return true;
        }
    }
}