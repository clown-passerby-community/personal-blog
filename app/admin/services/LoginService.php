<?php

namespace app\admin\services;

use app\common\exceptions\Exception;
use app\admin\model\Admins;
use app\common\services\Service;

class LoginService extends Service
{
    public static function login(string $admin_name, string $password)
    {
        $admin = Admins::getInstance()->getAdminByName($admin_name);
        if ( !$admin ) throw new Exception('管理员账户不存在！');
        if ( !hash_verify($password, $admin->password) ) throw new Exception('管理员信息不匹配！');
        switch ($admin->is_check) {
            case 0:
                throw new Exception('该管理员尚未启用！');
                break;
            case 2:
                throw new Exception('该管理员已禁用！');
                break;
        }
        return $admin;
    }
}