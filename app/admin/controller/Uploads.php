<?php
declare (strict_types = 1);

namespace app\admin\controller;

class Uploads extends Common
{
    public function file($file_name = 'file')
    {
        // $file: \think\File 对象
        $file = request()->file($file_name);

        // 读取磁盘配置名为public的所有配置项 返回数组
        $prefix  = \think\Facade\Filesystem::getDiskConfig(env('filesystem.driver'), 'url');

        // 上传到本地服务器
        $savename = $prefix . '/' . \think\facade\Filesystem::putFile('/', $file);

        return $this->successJson($savename);
    }

    public function files($file_name = 'files')
    {
        // $file: \think\File 对象
        $file = request()->file($file_name);

        // 上传到本地服务器
        $savename = \think\facade\Filesystem::putFile('/', $file);

        return $this->successJson($savename);
    }
}
