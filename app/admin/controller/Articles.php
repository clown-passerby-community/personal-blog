<?php
declare (strict_types = 1);

namespace app\admin\controller;

use app\admin\validate\ArticleValidate;
use think\App;
use think\facade\View;

class Articles extends Common
{
    protected $modelClass = \app\admin\model\Articles::class;
    protected $validator   = ArticleValidate::class;
    protected $withModel = ['menu'];
    protected $detailWithModel = ['labels'];

    public function __construct(App $app)
    {
        parent::__construct($app);

        // 文章分类列表
        View::assign('menu_list', \app\common\model\Menus::getInstance()->getSelectLists([], ['menu_sort' => 'ASC']));
    }

    protected function setSearchWhereFilter(array &$params = []):void
    {
        parent::setSearchWhereFilter($params); // TODO: Change the autogenerated stub

        if (isset($params['search']) && !empty($params['search'])){
            $params['where'][] = ['article_title|article_keywords|article_description', 'like', '%' . $params['search'] . '%'];
        }

        if ( isset($params['menu_id']) && $params['menu_id'] > -1 ) {
            $params['where'][] = ['menu_id', 'IN', \app\admin\model\Menus::getInstance()->getChildIds(intval($params['menu_id']))];
        }

        if ( isset($params['is_public']) && $params['is_public'] > -1 ) {
            $params['where'][] = ['is_public', '=', $params['is_public']];
        }

        if ( isset($params['set_top']) && $params['set_top'] > -1 ) {
            $params['where'][] = ['set_top', '=', $params['set_top']];
        }

        if ( isset($params['is_recommend']) && $params['is_recommend'] > -1 ) {
            $params['where'][] = ['is_recommend', '=', $params['is_recommend']];
        }
    }

    public function detail()
    {
        // 所有的文章标签列表
        View::assign('article_labels', \app\common\model\Article\ArticleLabels::getInstance()->getSelectLists());

        return parent::detail(); // TODO: Change the autogenerated stub
    }
}
