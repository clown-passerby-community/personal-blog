<?php
declare (strict_types = 1);

namespace app\admin\validate;

use app\common\model\Article\ArticleCategories;
use app\common\validate\BaseValidate;

class ArticleCategoryValidate extends BaseValidate
{
    /**
     * 定义错误信息
     * 格式：'字段名.规则名' =>  '错误信息'
     *
     * @var array
     */
    protected $message = [
        'category_id.require'   => 'Id为必填项！',
        'category_name.require' => '分类名称为必填项！',
        'category_name.unique' => '分类名称已存在，请更换！',
    ];
    /**
     * 验证场景定义
     *
     * @var array
     */
    protected $scene = [
        'create' => ['category_name'],
        'update' => [
            'category_id',
            'category_name',
        ],
    ];

    public function setRules()
    {
        $model = ArticleCategories::getInstance();
        $pk = $model->getPk();
        $validate_id = ',' . input($pk, 0) . ',' .  $pk;

        return [
            'category_id'    => 'require',
            'category_name'  => [
                'require',
                'max:200',
                'unique:' . $model->getName() . ',category_name^is_delete' . $validate_id
            ],
        ];
    }
}
