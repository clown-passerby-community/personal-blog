<?php
declare (strict_types = 1);

namespace app\admin\validate;

use app\admin\model\Admins;
use app\common\validate\BaseValidate;

class AdminValidate extends BaseValidate
{
    /**
     * 定义错误信息
     * 格式：'字段名.规则名' =>  '错误信息'
     *
     * @var array
     */
    protected $message = [
        'admin_id.require'    => '管理员Id为必填项！',
        'admin_name.require'  => '管理员名称为必填项！',
        'admin_name.unique' => '管理员名称已存在，请更换！',
        'admin_email.require' => '管理员邮箱为必填项！',
        'admin_email.email'   => '管理员邮箱格式非法！',
        'is_check.number'     => '状态为必选项！',
    ];
    /**
     * 验证场景定义
     *
     * @var array
     */
    protected $scene = [
        'create' => [
            'admin_name',
            'admin_email',
            'is_check',
        ],
        'update' => [
            'admin_id',
            'admin_name',
            'admin_email',
            'is_check',
        ],
    ];

    public function setRules()
    {
        $model = Admins::getInstance();
        $pk = $model->getPk();
        $validate_id = ',' . input($pk, 0) . ',' .  $pk;

        return [
            'admin_id'    => 'require',
            'admin_name'  => [
                'require',
                'max:50',
                'unique:' . $model->getName() . ',admin_name^is_delete' . $validate_id
            ],
            'admin_email' => 'require|max:50|email',
            'is_check'    => 'number',
        ];
    }
}
