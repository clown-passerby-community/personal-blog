<?php
declare (strict_types = 1);

namespace app\admin\validate;

use app\common\model\HelpCenters;
use app\common\model\Versions;
use app\common\validate\BaseValidate;

class VersionValidate extends BaseValidate
{
    /**
     * 定义错误信息
     * 格式：'字段名.规则名' =>  '错误信息'
     *
     * @var array
     */
    protected $message = [
        'version_id.require'    => 'Id为必填项！',
        'version_name.require'  => '版本名称为必填项！',
        'version_number.require' => '版本号为必填项！',
        'version_number.unique' => '版本号已存在，请更换！',
        'version_content.require'   => '版本更新内容为必填项！',
    ];
    /**
     * 验证场景定义
     *
     * @var array
     */
    protected $scene = [
        'create' => [
            'version_name',
            'version_number',
            'version_content',
        ],
        'update' => [
            'version_id',
            'version_name',
            'version_number',
            'version_content',
        ],
    ];

    public function setRules()
    {
        $model = Versions::getInstance();
        $pk = $model->getPk();
        $validate_id = ',' . input($pk, 0) . ',' .  $pk;

        return [
            'version_id'    => 'require',
            'version_name'  => 'require|max:100',
            'version_number'  => [
                'require',
                'max:200',
                'unique:' . $model->getName() . ',version_number^is_delete' . $validate_id
            ],
            'version_content'   => 'require',
        ];
    }
}
