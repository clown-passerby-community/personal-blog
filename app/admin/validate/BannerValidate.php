<?php
declare (strict_types = 1);

namespace app\admin\validate;

use app\common\model\System\Banners;
use app\common\validate\BaseValidate;

class BannerValidate extends BaseValidate
{
    /**
     * 定义错误信息
     * 格式：'字段名.规则名' =>  '错误信息'
     *
     * @var array
     */
    protected $message = [
        'banner_id.require'    => 'Id为必填项！',
        'banner_title.require' => 'Banner标题为必填项！',
        'banner_title.max'     => 'Banner标题最大长度为50字符！',
        'banner_title.unique' => 'Banner标题已存在，请更换！',
        'banner_cover.require' => 'Banner封面为必填项！',
    ];
    /**
     * 验证场景定义
     *
     * @var array
     */
    protected $scene = [
        'create' => [
            'banner_title',
            'banner_cover',
        ],
        'update' => [
            'banner_id',
            'banner_title',
            'banner_cover',
        ],
    ];

    public function setRules()
    {
        $model = Banners::getInstance();
        $pk = $model->getPk();
        $validate_id = ',' . input($pk, 0) . ',' .  $pk;

        return [
            'banner_id'      => 'require',
            'banner_title'  => [
                'require',
                'max:200',
                'unique:' . $model->getName() . ',banner_title^is_delete' . $validate_id
            ],
            'banner_cover'   => 'require',
        ];
    }
}
