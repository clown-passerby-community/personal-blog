<?php
declare (strict_types = 1);

namespace app\admin\validate;

use app\common\model\Article\ArticleLabels;
use app\common\validate\BaseValidate;

class ArticleLabelValidate extends BaseValidate
{
    /**
     * 定义错误信息
     * 格式：'字段名.规则名' =>  '错误信息'
     *
     * @var array
     */
    protected $message = [
        'label_id.require'   => 'Id为必填项！',
        'label_name.require' => '标签名称为必填项！',
        'label_name.max'     => '标签名称最大长度为200字符！',
        'label_name.unique' => '标签名称已存在，请更换！',
    ];
    /**
     * 验证场景定义
     *
     * @var array
     */
    protected $scene = [
        'create' => ['label_name'],
        'update' => [
            'label_id',
            'label_name',
        ],
    ];

    public function setRules()
    {
        $model = ArticleLabels::getInstance();
        $pk = $model->getPk();
        $validate_id = ',' . input($pk, 0) . ',' .  $pk;

        return [
            'label_id'    => 'require',
            'label_name'  => [
                'require',
                'max:200',
                'unique:' . $model->getName() . ',label_name^is_delete' . $validate_id
            ],
        ];
    }
}
