<?php
declare (strict_types = 1);

namespace app\admin\validate;

use app\common\model\System\Friendlinks;
use app\common\validate\BaseValidate;

class FriendlinkValidate extends BaseValidate
{
    /**
     * 定义错误信息
     * 格式：'字段名.规则名' =>  '错误信息'
     *
     * @var array
     */
    protected $message = [
        'link_id.require'    => '文章Id为必填项！',
        'link_name.require'  => '友情链接站点名称为必填项！',
        'link_name.unique' => '友情链接站点名称已存在，请更换！',
        'link_cover.require' => '站点图标为必传项！',
        'link_url.require'   => '站点网址为必填项！',
    ];
    /**
     * 验证场景定义
     *
     * @var array
     */
    protected $scene = [
        'create' => [
            'link_name',
            'link_cover',
            'link_url',
        ],
        'update' => [
            'link_id',
            'link_name',
            'link_cover',
            'link_url',
        ],
    ];

    public function setRules()
    {
        $model = Friendlinks::getInstance();
        $pk = $model->getPk();
        $validate_id = ',' . input($pk, 0) . ',' .  $pk;

        return [
            'link_id'    => 'require',
            'link_name'  => [
                'require',
                'max:200',
                'unique:' . $model->getName() . ',link_name^is_delete' . $validate_id
            ],
            'link_cover' => 'require',
            'link_url'   => 'require',
        ];
    }
}
