<?php
declare (strict_types = 1);

namespace app\admin\validate;

use app\common\validate\BaseValidate;

class SetAdminPasswordValidate extends BaseValidate
{
    /**
     * 定义验证规则
     * 格式：'字段名' =>  ['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [
        'old_password'     => 'require',
        'password'         => 'require|min:3,max:10',
        'password_confirm' => 'require|confirm:password',
    ];
    /**
     * 定义错误信息
     * 格式：'字段名.规则名' =>  '错误信息'
     *
     * @var array
     */
    protected $message = [
        'old_password.require'     => '请输入原始密码！',
        'password.require'         => '请输入新密码！',
        'password_confirm.require' => '请输入确认密码！',
        'password_confirm.confirm' => '两次输入密码不一致！',
    ];
}
