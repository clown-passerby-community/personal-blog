<?php
declare (strict_types = 1);

namespace app\admin\validate;

use app\admin\model\AdminRoles;
use app\common\validate\BaseValidate;

class AdminRoleValidate extends BaseValidate
{
    /**
     * 定义错误信息
     * 格式：'字段名.规则名' =>  '错误信息'
     *
     * @var array
     */
    protected $message = [
        'role_id.require'   => '角色Id为必填项！',
        'role_name.require' => '角色名称为必填项！',
        'role_name.unique' => '角色名称已存在，请更换！',
        'is_check.number'   => '状态为必选项！',
    ];
    /**
     * 验证场景定义
     *
     * @var array
     */
    protected $scene = [
        'create' => [
            'role_name',
            'is_check',
        ],
        'update' => [
            'role_id',
            'role_name',
            'is_check',
        ],
    ];

    public function setRules()
    {
        $model = AdminRoles::getInstance();
        $pk = $model->getPk();
        $validate_id = ',' . input($pk, 0) . ',' .  $pk;

        return [
            'role_id'    => 'require',
            'role_name'  => [
                'require',
                'max:50',
                'unique:' . $model->getName() . ',role_name^is_delete' . $validate_id
            ],
            'is_check'    => 'number',
        ];
    }
}
