<?php
declare (strict_types = 1);

namespace app\admin\validate;

use app\common\model\HelpCenters;
use app\common\validate\BaseValidate;

class HelpCenterValidate extends BaseValidate
{
    /**
     * 定义错误信息
     * 格式：'字段名.规则名' =>  '错误信息'
     *
     * @var array
     */
    protected $message = [
        'help_id.require'    => 'Id为必填项！',
        'help_title.require' => '标题为必填项！',
        'help_title.unique' => '标题已存在，请更换！',
    ];
    /**
     * 验证场景定义
     *
     * @var array
     */
    protected $scene = [
        'create' => ['help_title'],
        'update' => [
            'help_id',
            'help_title',
        ],
    ];

    public function setRules()
    {
        $model = HelpCenters::getInstance();
        $pk = $model->getPk();
        $validate_id = ',' . input($pk, 0) . ',' .  $pk;

        return [
            'help_id'    => 'require',
            'help_title'  => [
                'require',
                'max:200',
                'unique:' . $model->getName() . ',help_title^is_delete' . $validate_id
            ],
        ];
    }
}
