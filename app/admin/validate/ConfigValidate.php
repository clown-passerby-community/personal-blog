<?php
declare (strict_types = 1);

namespace app\admin\validate;

use app\admin\model\Configs;
use app\common\validate\BaseValidate;

class ConfigValidate extends BaseValidate
{
    /**
     * 定义错误信息
     * 格式：'字段名.规则名' =>  '错误信息'
     *
     * @var array
     */
    protected $message = [
        'config_id.require'    => '配置Id为必填项！',
        'config_title.require' => '配置标题为必填项！',
        'config_title.max'     => '配置标题最大长度为100字符！',
        'config_name.require'  => '配置名称为必填项！',
        'config_name.max'      => '配置名称最大长度为100字符！',
    ];
    /**
     * 验证场景定义
     *
     * @var array
     */
    protected $scene = [
        'create' => [
            'config_title',
            'config_name',
        ],
        'update' => [
            'config_id',
            'config_title',
            'config_name',
        ],
    ];

    public function setRules()
    {
        $model = Configs::getInstance();
        $pk = $model->getPk();
        $validate_id = ',' . input($pk, 0) . ',' .  $pk;

        return [
            'config_id'      => 'require',
            'config_title' => 'require|max:200',
            'config_name'  => [
                'require',
                'max:200',
                'unique:' . $model->getName() . ',config_name^is_delete' . $validate_id
            ],
        ];
    }
}
