<?php
declare (strict_types = 1);

namespace app\admin\validate;

use app\admin\model\Articles;
use app\common\validate\BaseValidate;

class ArticleValidate extends BaseValidate
{
    /**
     * 定义验证规则
     * 格式：'字段名' =>  ['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [
        'article_id'      => 'require',
        'menu_id'     => 'require',
        'article_title'   => 'require|max:200',
        'article_cover'   => 'require',
        'article_content' => 'require',
    ];
    /**
     * 定义错误信息
     * 格式：'字段名.规则名' =>  '错误信息'
     *
     * @var array
     */
    protected $message = [
        'article_id.require'      => 'Id为必填项！',
        'menu_id.require'     => '请选择文章所属栏目！',
        'article_title.require'   => '文章标题为必填项！',
        'article_title.unique' => '文章标题已存在，请更换！',
        'article_cover.require'   => '文章封面为必填项！',
        'article_content.require' => '文章内容为必填项！',
    ];
    /**
     * 验证场景定义
     *
     * @var array
     */
    protected $scene = [
        'create' => [
            'category_id',
            'article_title',
            'article_cover',
            'article_content',
        ],
        'update' => [
            'article_id',
            'category_id',
            'article_title',
            'article_cover',
            'article_content',
        ],
    ];

    public function setRules()
    {
        $model = Articles::getInstance();
        $pk = $model->getPk();
        $validate_id = ',' . input($pk, 0) . ',' .  $pk;

        return [
            'article_id'      => 'require',
            'menu_id'     => 'require',
            'article_title'  => [
                'require',
                'max:200',
                'unique:' . $model->getName() . ',article_title^is_delete' . $validate_id
            ],
            'article_cover'   => 'require',
            'article_content' => 'require',
        ];
    }
}
