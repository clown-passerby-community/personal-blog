<?php
declare (strict_types = 1);

namespace app\admin\validate;

use app\common\validate\BaseValidate;

class AdminMenuValidate extends BaseValidate
{
    /**
     * 定义验证规则
     * 格式：'字段名' =>  ['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [
        'menu_id'   => 'require',
        'menu_name' => 'require|max:50',
        'is_check'  => 'number',
    ];
    /**
     * 定义错误信息
     * 格式：'字段名.规则名' =>  '错误信息'
     *
     * @var array
     */
    protected $message = [
        'menu_id.require'   => '菜单Id为必填项！',
        'menu_name.require' => '菜单名称为必填项！',
        'is_check.number'   => '状态为必选项！',
    ];

    /**
     * 验证场景定义
     * @var array
     */
    protected $scene = [
        'create' => [
            'menu_name',
            'is_check',
        ],
        'update' => [
            'menu_id',
            'menu_name',
            'is_check',
        ],
    ];
}
