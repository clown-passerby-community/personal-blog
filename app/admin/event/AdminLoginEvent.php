<?php
declare (strict_types = 1);

namespace app\Admin\event;

class AdminLoginEvent
{
    public $admin;

    public function __construct($admin)
    {
        $this->admin = $admin;
    }
}
