<?php
declare (strict_types = 1);

namespace app\admin\model;

class Menus extends \app\common\model\Menus
{
    private function adminList()
    {
        return $this->deleteWhere()->order('menu_sort', 'ASC')->select();
    }

    public function getChildIds(int $parent_id, array $list = []): array
    {
        $arr_ids = [$parent_id];
        if (empty($list)) $list = $this->adminList()->toArray();
        foreach ($list as $key => $value){
            if ($value['parent_id'] == $parent_id) {
                unset($list[$key]);
                $sub_ids = $this->getChildIds($value['menu_id'], $list);
                !empty($sub_ids) && $arr_ids = array_merge($arr_ids, $sub_ids);
            }
        }
        return $arr_ids;
    }
}
