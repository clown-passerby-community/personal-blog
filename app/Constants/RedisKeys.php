<?php

namespace app\Constants;

class RedisKeys
{
    /**
     * 手机验证码的Key标识（List）
     */
    const SMSCODE_PREFIX = [
        'login' => 'user:login_sms', // 登录的验证码
        'register' => 'user:register_sms', // 注册的验证码
        'forget' => 'user:forget_sms', // 找回密码的验证码
    ];

    // users_token：token:会员基本信息（List）
    const USER_LOGIN_TOKEN = 'users_token:';

    // 暂定：hash，如果后续要变更类型，那么使用 有序集合
    //  user_labels:会员Id
    //  key:标签Id
    //  Value:标签的权重（就是关注多少相关标签的视频）
    const USER_LABELS = 'user_labels';

    // 会员在首页看过的视频Id组：hash
    //
    const USER_LOOK_DYNAMIC_IDS = 'user_look_dynamic_ids';

    const KEY_DEFAULT_TIMEOUT = 3600 * 24;
}