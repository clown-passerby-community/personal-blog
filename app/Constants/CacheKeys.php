<?php

namespace app\Constants;

class CacheKeys
{
    /**
     * 关注人的缓存key
     */
    // follow_user:user_id
    const FOLLOW_USER_KEY = 'follow_user';
    const FAN_USER_KEY = 'fan_user';
    const FRIEND_USER_KEY = 'friend_user';


    const KEY_DEFAULT_TIMEOUT = 3600;
}