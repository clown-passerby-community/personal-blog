<?php
namespace app;

// 应用请求对象类
class Request extends \think\Request
{
    public function __construct()
    {
        parent::__construct();

        // 追加参数
        $this->param = array_merge($this->param, ['is_delete' => 0]);
    }
}
