<?php
// 应用公共文件

include_once __DIR__ . '/functions.php';

/**
 * 写入日志 (使用tp自带驱动记录到runtime目录中)
 * @param $value
 * @param string $type
 */
function log_write($value, $type = 'info')
{
    $msg = is_string($value) ? $value : var_export($value, true);
    \think\facade\Log::record($msg, $type);
}

function cnpscy_config($config_name = '')
{
    return config('cnpscy' . (empty($config_name) ? '' : '.') . $config_name);
}

function getLoginAdmin()
{
    return \think\facade\Session::get('login_admin');
}

function getLoginAdminId()
{
    return getLoginAdmin()->admin_id;
}