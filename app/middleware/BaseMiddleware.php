<?php
declare (strict_types = 1);

namespace app\middleware;

use app\Traits\Json;

class BaseMiddleware
{
    use Json;
}
