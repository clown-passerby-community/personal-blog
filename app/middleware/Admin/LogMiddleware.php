<?php
declare (strict_types = 1);

namespace app\middleware\Admin;

use app\common\model\Rabc\AdminLogs;
use think\facade\Log;

class LogMiddleware
{
    // 过滤的路由：不进行检测的路由
    protected $filter_route = [
        'adminlogs/delete',
    ];

    /**
     * 处理请求
     *
     * @param  \think\Request  $request
     * @param  \Closure        $next
     *
     * @return Response
     */
    public function handle($request, \Closure $next)
    {
        $result = $next($request);

        // 是否开启管理员日志
        $method = $request->method();
        if ( intval(cnpscy_config('start_admin_log')) == 1 && $method != 'GET' ) {
            $route = $request->controller(true) . '/' . $request->action();
            // 只要路由不再过滤路由属性值中，那么就自动记录日志
            if ( !isset(array_flip($this->filter_route)[$route]) ) {
                AdminLogs::getInstance()->record(getLoginAdminId(), $method);
            }
        }

        return $result;
    }
}
