<?php
declare (strict_types = 1);

namespace app\middleware\Admin;

use app\middleware\BaseMiddleware;
use think\facade\Session;

class AuthMiddleware extends BaseMiddleware
{
    /**
     * 处理请求
     *
     * @param \think\Request $request
     * @param \Closure       $next
     * @return Response
     */
    public function handle($request, \Closure $next)
    {
        // 检测登录管理员的基本信息
        if (!getLoginAdmin()){
            if ($request->isPost()){
                return $this->errorJson('请重新登录（Auth）', -1);
            }else{
                return redirect('/admin/logins/index');
            }
        }

        return $next($request);
    }
}
