<?php

namespace Storage\engine;

use think\facade\Filesystem;

/**
 * 本地文件驱动
 * Class Local
 *
 * @package app\common\library\storage\drivers
 */
class Local extends Server
{
    protected $config;
    public function __construct()
    {
        parent::__construct();

        // 上传目录
        $this->config = Filesystem::getDiskConfig(env('filesystem.driver'));
    }

    /**
     * 上传图片文件
     * @return array|bool
     */
    public function upload($dir='')
    {
        return $this->isInternal ? $this->uploadByInternal($dir) : $this->uploadByExternal($dir);
    }

    /**
     * 外部上传(指用户上传,需验证文件类型、大小)
     * @return bool
     */
    private function uploadByExternal($dir='')
    {
        $uplodDir = $this->config['root'];
        if($dir){
            $uplodDir.='/'.$dir;
        }
        // 验证文件并上传
        $info = $this->file->validate([
            'size' => 4 * 1024 * 1024,
            'ext' => 'jpg,jpeg,png,mp4,avi', // 新增视频验证
        ])->move($uplodDir, $this->fileName);
        if (empty($info)) {
            $this->error = $this->file->getError();
            return false;
        }
        return true;
    }

    /**
     * 内部上传(指系统上传,信任模式)
     * @return bool
     */
    private function uploadByInternal($dir='')
    {
        // 上传目录
        $uplodDir = $this->config['root'];
        if($dir){
            $uplodDir.='/'.$dir;
        }
        // 要上传图片的本地路径
        $realPath = $this->getRealPath();
        if (!rename($realPath, "{$uplodDir}/$this->fileName")) {
            $this->error = 'upload write error';
            return false;
        }
        return true;
    }

    /**
     * 删除文件
     * @param $fileName
     * @return bool|mixed
     */
    public function delete($fileName)
    {
        // 文件所在目录
        $filePath = $this->config['root'] . "/{$fileName}";
        return !file_exists($filePath) ?: unlink($filePath);
    }

    /**
     * 返回文件路径
     * @return mixed
     */
    public function getFileName()
    {
        return $this->fileName;
    }

}
