<?php
/**
 * Created by IntelliJ IDEA.
 * User: pc
 * Date: 2020/10/21
 * Time: 17:11
 * Note: Redis.php
 */

namespace Redis;

use think\facade\Config;

//  extends \Redis

class Redis
{
    //创建静态私有的变量保存该类对象
    static private $instance;

    public function __construct()
    {
    }

    //防止使用clone克隆对象
    private function __clone(){

    }

    static public function getInstance()
    {
        $options = Config::get('queue.connections.redis');
        $redis = new \Redis;
        $redis->connect($options['host'], $options['port'], $options['timeout']);

        if ('' != $options['password']) {
            $redis->auth($options['password']);
        }

        if (0 != $options['select']) {
            $redis->select($options['select']);
        }
        return $redis;
    }
}